// Requisito:

// input: [obj1, obj2, obj3... objn]

// obj = {
//     name: "string",
//     priority: 1..n (random)
// }

// O algoritmo deve ordenar usando quicksort e retornar o seguinte: "String (1), String (2).. "


// Metodologia:

// 1) Criar o objeto OBJ, contendo uma string String e um codigo Priority
//done

// 2) Criar o método "Randomizador" para pegar um código de priority 


// 3) Criar o método construtor de OBJ 
//done

// 4) Criar o método quicksort


// 5) Criar a variavel ARRAY para armazenar OBJs..a


 //FUNCTION BUILD RANDOM ARRAY
 function buildRandomArray(numberOfElements){
     let i;
     let randomArray = [];
    
     for (i = 0; i < numberOfElements; i++ ){

        let randomObj = {
            name: `Item ${i+1}`,
            priority: Math.floor((Math.random() * 100) + 1)
        };

        randomArray[i] = randomObj;
     }

     return randomArray;
 }

//FUNCTION PRINT ARRAY
function printArray(arrayToBePrinted){
    let i;

    for (i = 0; i < arrayToBePrinted.length - 1; i++){
        console.log(arrayToBePrinted[i].name + " | Priority: " + arrayToBePrinted[i].priority);
    }
}

//FUNCTION SWAP ELEMENTS
function swapElements(arrayOfElements, firstIndex, secondIndex){
    let temp = arrayOfElements[firstIndex];
    arrayOfElements[firstIndex] = arrayOfElements[secondIndex];
    arrayOfElements[secondIndex] = temp;
}

//FUNCTION CREATE PARTITION
function quicksortPartition(arrayOfElements, pivot, leftIndex, rightIndex){
    
    let pivotIndex = arrayOfElements[pivot];
    let pivotValue = arrayOfElements[pivot].priority;

    let partitionIndex = leftIndex;

    let i;

    for (i = leftIndex; i < rightIndex; i++ ){
        if(arrayOfElements[i].priority < pivotValue){
            swapElements(arrayOfElements, i, partitionIndex);
            partitionIndex++;
        }
    }
    
    swapElements(arrayOfElements, rightIndex, partitionIndex);
    return partitionIndex;
}

//QUICKSORT METHOD
function quickSortMethod(disorderedArray, leftIndex, rightIndex){

    if(leftIndex < rightIndex){
        let pivotIndex = rightIndex;
        let partitionIndex = quicksortPartition(disorderedArray, pivotIndex, leftIndex, rightIndex); 
    
        quickSortMethod(disorderedArray, leftIndex, partitionIndex - 1);
        quickSortMethod(disorderedArray, partitionIndex + 1, rightIndex);
    }

    return disorderedArray;
}


//DEFINA DENTRO DE buildRandomArray O FUCKING TAMANHO DA BUCETA DO CARALHO DO PRIQUITO DESTE VETOR DE MERDA

let arrayUnordered = buildRandomArray(20);
let leftIndex = 0;
let rightIndex = arrayUnordered.length -1;

printArray(arrayUnordered);

console.log("Segue array organizado: ");

printArray(quickSortMethod(arrayUnordered, leftIndex, rightIndex));
